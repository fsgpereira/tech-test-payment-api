namespace tech_test_payment_api.Models
{
    public enum StatusPedido
    {
        AguardandoPagamento = 1,
        PagamentoAprovado,
        PedidoCancelado,
        PedidoEntregue,
        EnviadoTransportadora
    }
}