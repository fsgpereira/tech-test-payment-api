using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("Controller")]
    public class ProdutoController : ControllerBase
    {
        public static List<Produto> ListaProduto;

        public ProdutoController()
        {
            if (ListaProduto == null)
            {
                ListaProduto = new List<Produto>();

                var v1 = new Produto();
                v1.Nome = "Cafeteira";
                v1.Valor = 205.00M;

                var v2 = new Produto();
                v2.Nome = "Britadeira";
                v2.Valor = 2580.99M;

                var v3 = new Produto();
                v3.Nome = "Colheitadeira";
                v3.Valor = 32120.00M;

                ListaProduto.Add(v1);
                ListaProduto.Add(v2);
                ListaProduto.Add(v3);
            }
        }

        [HttpGet("Products/")]
        public IActionResult Index()
        {
            return Ok(ListaProduto);
        }

        [HttpPost("Products/Create/")]
        public IActionResult Create(Produto produto)
        {
            if (produto != null)
            {
                ListaProduto.Add(produto);
                return Ok(produto);
            }

            return NoContent();
        }

        [HttpPut("Products/Edit/")]
        public IActionResult Edit(Produto produto)
        {
            var ret = ListaProduto.Find(x => x.Id == produto.Id);

            if (ret != null)
            {
                ret.Nome = produto.Nome;
                ret.Valor = produto.Valor;
            }
            else
            {
                return NotFound();
            }
            return Ok(produto);
        }

        [HttpDelete("Products/Delete/")]
        public IActionResult Delete(Guid id)
        {
            var ret = ListaProduto.Find(x => x.Id == id);
            if (ret == null)
            {
                return NotFound();
            }

            ListaProduto.Remove(ret);
            return NoContent();
        }
    }
}