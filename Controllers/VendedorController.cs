using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("Controller")]
    public class VendedorController : ControllerBase
    {
        public static List<Vendedor> ListaVendedores;

        public VendedorController()
        {
            if (ListaVendedores == null)
            {
                ListaVendedores = new List<Vendedor>();

                var v1 = new Vendedor();
                v1.CPF = "123.456.789-10";
                v1.Nome = "Roberto Carlos";
                v1.Email = "robertoCarlos@hotmail.com";
                v1.Telefone = "15-9911001100";

                ListaVendedores.Add(v1);

                var v2 = new Vendedor();
                v2.CPF = "123.123.123-00";
                v2.Nome = "Martinho da Vila";
                v2.Email = "martinhoVila@hotmail.com";
                v2.Telefone = "65-7788445511";

                ListaVendedores.Add(v2);

                var v3 = new Vendedor();
                v3.CPF = "130.456.789-00";
                v3.Nome = "Silvio Santos";
                v3.Email = "Silvio@hotmail.com";
                v3.Telefone = "18-9955887755";

                ListaVendedores.Add(v3);
            }
        }

        [HttpGet("Sellers/")]
        public IActionResult Index()
        {
            return Ok(ListaVendedores);
        }

        [HttpPost("Sellers/Create/")]
        public IActionResult Create(Vendedor vendedor)
        {
            if (vendedor != null)
            {
                ListaVendedores.Add(vendedor);
                return Ok(vendedor);
            }

            return NoContent();
        }

        [HttpPut("Sellers/Edit/")]
        public IActionResult Edit(Vendedor vendedor)
        {
            var ret = ListaVendedores.Find(x => x.Id == vendedor.Id);

            if (ret != null)
            {
                ret.CPF = vendedor.CPF;
                ret.Nome = vendedor.Nome;
                ret.Email = vendedor.Email;
                ret.Telefone = vendedor.Telefone;
            }
            else
            {
                return NotFound();
            }
            return Ok(vendedor);
        }

        [HttpDelete("Sellers/Delete/")]
        public IActionResult Delete(Guid id)
        {
            var ret = ListaVendedores.Find(x => x.Id == id);
            if (ret == null)
            {
                return NotFound();
            }

            ListaVendedores.Remove(ret);
            return NoContent();
        }
    }
}